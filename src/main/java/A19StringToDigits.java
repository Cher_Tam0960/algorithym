
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author kitti
 */
public class A19StringToDigits {
//        A-1.9 Given a string, S, of n digits in the range from 0 to 9, 
//        describe an efficientalgorithm for converting S into the integer 
//        it represents. 

//        What is the running time of your algorithm?
    public static void main(String[] args) throws IOException {
        long startTime = System.nanoTime();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String numString = reader.readLine();

        char[] num = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
        int Total = 1;
        int numj;
        for (int i = 0; i < numString.length(); i++) {
            for (int j = 0; j < num.length; j++) {
                if (numString.charAt(i) == num[j]) {
                    numj = j;
                    for (int k = 0; k < numString.length() - i-1; k++) {
                        numj *= 10;
                    }
                    Total += numj;
                }
            }
        }
        Total = Total -1;
        System.out.println("A number of " + numString + " is " + Total);

        long endTime = System.nanoTime();
        long totalTime = endTime - startTime;

        double seconds = (double) totalTime / 1000000000;
        System.out.println("Total time of this program is " + seconds);

    }
}
