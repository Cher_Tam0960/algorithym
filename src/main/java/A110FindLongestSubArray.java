
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author kitti
 */
public class A110FindLongestSubArray {
//           A-1.10 Given an array, A, of n integers, 
//           find the longest subarray of A such that all the numbers in that 
//           subarray are in sorted order. 

//           What is the running time of yourmethod?
    public static void main(String[] args) throws IOException {
        long startTime = System.nanoTime();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String inputArrString;
        String arrSize = reader.readLine();

        int size = Integer.parseInt(arrSize);
        Integer[] A = new Integer[size];
        
        

        String num;
        int stringToNum;

        for (int i = 0; i < A.length; i++) {
            num = reader.readLine();
            stringToNum = Integer.parseInt(num);
            A[i] = stringToNum;
        }

        int max = 1;
        int lenth = 1;

        for (int i = 1; i < A.length; i++) {
            if (A[i] > A[i - 1]) {
                lenth++;
            } else {
                if (max < lenth) {
                    max = lenth;
                }
                lenth = 1;
            }
        }
        

        System.out.println("input array is " + Arrays.toString(A));
        System.out.println("The longest subarray is " + max);
        
        long endTime = System.nanoTime();
        long totalTime = endTime - startTime;

        double seconds = (double) totalTime / 1000000000;
        System.out.println("Total time of this program is " + seconds);
    }
}
